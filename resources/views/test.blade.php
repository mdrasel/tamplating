@extends('test.test')
@section('title','Test')
@section('right')
    <h2>SALIENT STRENGTH OF MUTUAL</h2>
    <ul class="list">
    <li>Proven track record of being trusted partner for Global companies in Bangladesh operation.</li>
    <li>Ability to manufacture Global Brands with strict adherence to GMP and prescribed QC guidelines.</li>
    <li>Nationwide strong logistics and distribution footprint.</li>
    <li>Nationwide exclusive retail channel distribution network.</li>
    <li>Sound financial background.</li>
    <li>Application knowledge of best practice, Standard Operating Procedure (SOP) & Regulatory Compliance.</li>
    <li>People of very positive attitude and commitment.</li>
    </ul>
    @endsection
@section('left')
    <h2>Mutual Group</h2>
    <p>Mutual Group is primarily a 'Toll Manufacturing & Distribution company' with the legacy of establishing two global brands,
        'Horlicks' and 'Dano', as the household name in Bangladesh. It started its journey as trading company back in 1962.</p>
    <p>Over the period of 50 years it has expanded its capability in Manufacturing, Logistics and Retail distribution and have
        become one of the largest 'Extended Supply Chain Company' in Bangladesh. No other company in Bangladesh carries such a long
        and consistent track record of upholding global MNCs business interest in Bangladesh market.</p>
    @endsection