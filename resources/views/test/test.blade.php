<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{!! asset('Assets/css/style.css') !!}">
    <link rel="icon" href="{!! asset('Assets/images/logo2.jpg') !!}">
</head>
<body>
<div id="main">
@include('test.subcontent.header')
    <div id="menu">
@include('test.subcontent.menu')
    </div>
    <div id="slidder"><img src="{!! asset('Assets/images/dano.jpg') !!}" alt="arlafood"></div>
    <div id="slogan">
      @include('test.subcontent.marquee')
    </div>
    <div id="content_main">
        <div id="left">
            @yield('left')
        </div>
        <div id="right">
                @yield('right')
            </ul>
        </div>
    </div>
    <div id="readmore">
        <div id="left"><a href="Assets/common/under_construction.html"><span>Read More</span></a></div>
        <div id="right"><a href="Assets/common/under_construction.html"><span>Read More</span></a></div>
    </div>
    <div id="line"></div>
    <div id="footer">
@include('test.subcontent.footer')
    </div>
</div>
</body>
</html>
